package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.domain.Client;
import com.example.domain.Product;
import com.example.services.ProductService;

@RestController
public class RestProductController {

	@Autowired
	ProductService productService;
	
	@RequestMapping("/product")
	public ResponseEntity<List<Product>> findAllProducts(){
		
		HttpHeaders header=new HttpHeaders();
		return new ResponseEntity<List<Product>>(productService.findAllProducts(),
												 header,
												 HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/product/{id}/edit")
	public ResponseEntity<Product> findClientById(@PathVariable Long id){
		HttpHeaders header= new HttpHeaders();
		return new ResponseEntity<Product>(productService.findProductByID(id),
										  header,
										  HttpStatus.CREATED); 
	}
	
	@RequestMapping(path="/product/update",method=RequestMethod.PATCH)
	public ResponseEntity<Product> updateClient(@RequestBody Product product){
		System.out.println(product.getId()+" "+
				product.getProductName()+" "+
				product.getDescription()+" "+
				product.getReleaseDate());
	
		productService.updateProduct(product.getId(),product.getProductName(),
									product.getBasePrice(),
									product.getDescription(),product.getReleaseDate());
		HttpHeaders header=new HttpHeaders();
		return new ResponseEntity<Product>(productService.findProductByID(product.getId()),
				  header,
				  HttpStatus.CREATED);
		
	}
	
	@RequestMapping(path="product/new", method=RequestMethod.POST)
	public void createNewProduct(@RequestBody Product product){
		System.out.println(product.getProductName()+" "+product.getBarcode()+" "
				+product.getBasePrice()+" "+product.getReleaseDate());
	}
	
	
	
}
