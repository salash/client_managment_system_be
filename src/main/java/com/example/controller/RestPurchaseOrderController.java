package com.example.controller;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.domain.PurchaseOrder;
import com.example.services.PurchaseOrderService;

@RestController
public class RestPurchaseOrderController {
	@Autowired
	PurchaseOrderService POService;
	
	@RequestMapping("/orders")
	public ResponseEntity<List<PurchaseOrder>> getAllPurchaseOrders(){
		HttpHeaders header=new HttpHeaders();	
		return new ResponseEntity<List<PurchaseOrder>>(POService.getAllPO(),
														 header,
														 HttpStatus.CREATED);
	} 

	
}
