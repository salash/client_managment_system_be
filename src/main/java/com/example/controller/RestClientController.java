package com.example.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.domain.Client;
import com.example.services.ClientService;

@RestController
public class RestClientController {   
	
	@Autowired
	ClientService clientService;
	
	
	@RequestMapping(value="/client")
	public ResponseEntity<List<Client>> showAllClinets(){

		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<List<Client>>(clientService.findAllClients(),
												headers,
												HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/client/{id}/edit")
	public ResponseEntity<Client> findClientById(@PathVariable Long id){
		HttpHeaders header= new HttpHeaders();
		return new ResponseEntity<Client>(clientService.findClientByID(id),
										  header,
										  HttpStatus.CREATED); 
	}
	
	@RequestMapping(path="/client/update",method=RequestMethod.PATCH)
	public ResponseEntity<Client> updateClient(@RequestBody Client client){
		System.out.println(client.getFirstname()+" "+client.getLastname()+" "+client.getPhone()+" "+client.getCountry());
		clientService.updateClient(client.getId(),client.getFirstname(),client.getLastname(),client.getPhone(),client.getAddress(),client.getCountry());
		HttpHeaders header=new HttpHeaders();
		return new ResponseEntity<Client>(clientService.findClientByID(client.getId()),
				  header,
				  HttpStatus.CREATED);
		
	}
}
