package com.example.domain;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity
@Data
public class PurchaseOrder {
	
	@Id
	@GeneratedValue
	Long id;
	
	@OneToOne
	Client client;
	
	@OneToOne
	Product product;
	
	LocalDate transactionDate;
	Double convertedPrice;

	static int orderId=1;
	int po_orderId;
	PurchaseOrder(){
		this.po_orderId=PurchaseOrder.orderId;
		PurchaseOrder.orderId++;
	}
	
}
