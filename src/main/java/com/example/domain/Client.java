package com.example.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Client {
	@Id
	@GeneratedValue
	Long id;

	String security_No;
	String firstname;
	String lastname;
	String phone;
	String address;
	String country;
	
	public void setClient(String firstname,String lastname,String phone,String address,String country){
		this.firstname=firstname;
		this.lastname=lastname;
		this.phone=phone;
		this.address=address;
		this.country=country;
	}
}
