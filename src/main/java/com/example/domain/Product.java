package com.example.domain;

import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;

@Entity
@Data
public class Product {
	@Id
	@GeneratedValue
	Long id;
	String productName;
	String barcode;
	Double basePrice;
	String description;
	LocalDate releaseDate;	
	
	public void setProduct(  String productName,
							 Double basePrice,
							String description,LocalDate releaseDate){
		this.productName=productName;
		this.basePrice=basePrice;
		this.description=description;
		this.releaseDate=releaseDate;
	}
}
