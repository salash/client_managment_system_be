package com.example.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.domain.Client;
import com.example.repository.ClientRepository;

@Service
public class ClientService {
	
	@Autowired
	ClientRepository clientRepo; 
	
	public List<Client> findAllClients(){
		return clientRepo.findAll();
	}
	
	public Client findClientByID(Long id){
		return clientRepo.findByid(id);
	}

	public void updateClient(Long id, String firstname, String lastname, String phone,String address, String country) {
		Client client=clientRepo.findByid(id);
		client.setClient(firstname, lastname, phone, address,country);
		clientRepo.save(client);
	}

}
