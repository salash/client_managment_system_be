package com.example.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.domain.PurchaseOrder;
import com.example.repository.PurchseOrderRepository;

@Service
public class PurchaseOrderService {
	@Autowired
	PurchseOrderRepository PORepo;
	
	public List<PurchaseOrder> getAllPO(){
		return PORepo.findAll();
	}
}
