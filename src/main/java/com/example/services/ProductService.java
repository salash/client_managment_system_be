package com.example.services;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.domain.Client;
import com.example.domain.Product;
import com.example.repository.ProductRepository;

@Service
public class ProductService {
	@Autowired
	ProductRepository productRepo;
	
	@RequestMapping("/products")
	public List<Product> findAllProducts(){
		return productRepo.findAll();
	}

	public Product findProductByID(Long id){
		return productRepo.findByid(id);
	}

	public void updateProduct(Long id, String productName, Double basePrice, String description,
			LocalDate releaseDate) {
		System.out.println("in Service: "+id+" "+
				productName+" "+
				description+" "+
				basePrice);
		
		Product pro=productRepo.findByid(id);
		pro.setProduct(productName, basePrice, description, releaseDate);
		productRepo.save(pro);
		
		
	}
	
	
}
